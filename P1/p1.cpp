/*
Matheus Mendes Araujo 156737

  Para a resolução do exercício, eu implementei o algoritmo
  de ordenação MergeSort e fiz uma alteração para contabilizar
  o número de inversões. Esse algoritmo de ordenação foi escolhido
  e implementado, pois possui complexidade de O(n log(n)) e utiliza
  o conceito de divisão e conquista, visto em aula.
*/

#include <iostream>
#include <vector>
#include <stdio.h>
#include <math.h>
using namespace std;


typedef unsigned long long int ulong;
//variável global para contagem de inversões
ulong cont = 0;

void print(vector<int> v){
  for (ulong i = 0; i < v.size(); i++) {
    printf("%d ", v[i] );
  }
  printf("\n");
}

//função para realizar a intercalação dos vetores
void Intercala(vector<int> &v, int ini, int med, int fim){
    vector<int> I, F;
    int i, j, k;

    //redimensiona o vector
    I.resize(med -ini +1);
    F.resize(fim - med);

    //insereve valores no vetores aux
    for (i = 0; i < I.size(); i++){
      I[i] = v[i + ini];
    }

    for (j = 0; j < F.size(); j++){
      F[j] = v[j+med+1];
    }

    //Intercala vetores
    for(i = 0, j = 0, k = ini; k < fim && i < I.size() && j < F.size(); k++ ){
      if (I[i] <= F[j]){
        v[k] = I[i++];
      }
      else{
        v[k] = F[j++];
        //contabiliza as inversões que foram feitas
        cont = cont + (I.size() -i);
      }
    }

    //recebe os valores faltantes dos vetores
    while(i < I.size()){
      v[k++] = I[i++];
    }
    while(j < F.size()){
      v[k++] = F[j++];
    }
}

//função MergeSort
void MergeSort(vector<int> &v, int ini, int fim){
  if (ini < fim){
    int med =  (ini + fim)/2;
    MergeSort(v,ini,med);
    MergeSort(v,med + 1,fim);
    Intercala(v,ini,med,fim);
  }
}

int main() {
  //variáveis
  ulong size_vet = 0;
  vector<int> v;

  //recebe tamanho vector
  cin >> size_vet;
  //redimensiona o vector
  v.resize(size_vet);

  //recebe valores do vector
  for (ulong i = 0; i < v.size(); i++) {
    cin >> v[i];
  }

  //chama o MergeSort
  MergeSort(v, 0, (size_vet - 1) );
  //o número de inversão
  printf("%llu\n", cont );
}
